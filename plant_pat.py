import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import tensorflow_addons as tfa
import tensorflow.keras.callbacks as C
from tensorflow.keras.models import Model
from tensorflow.keras.applications.densenet import DenseNet201
from tensorflow.keras.models import Sequential, load_model, save_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array, smart_resize
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, \
    Flatten, BatchNormalization, Activation, GlobalAveragePooling2D, Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import AUC

PATH = r'X:\Python\PycharmProjects\plant\plant-pathology-2021-fgvc8/'

log_dir = r'X:\Python\PycharmProjects\plant\plant-pathology-2021-fgvc8\log_dir'
weights = r'X:\Python\PycharmProjects\plant\plant-pathology-2021-fgvc8' \
          r'\densenet201_weights_tf_dim_ordering_tf_kernels_notop.h5 '

train_img = os.path.join(PATH, 'train_images/')
test_img = os.path.join(PATH, 'test_images/')

train_data = os.path.join(PATH, 'train.csv')
test_data = os.path.join(PATH, 'sample_submission.csv')

train = pd.read_csv(train_data)
test = pd.read_csv(test_data)

labels = train.labels.unique()
value_counts = train['labels'].value_counts()

train_copy = train
train_copy = train_copy['labels'].str.split(' ', expand=True).stack()
labels_bin = pd.get_dummies(train_copy).groupby(level=0).sum()

labels_bin.head()

cols = labels_bin.columns
label_counts = labels_bin[cols].sum()

plt.figure(figsize=(10, 6))
plt.title("Comparison of all unique Labels")
sns.barplot(x=cols, y=label_counts)

train['labels'] = train['labels'].str.split(' ')

datagen = ImageDataGenerator(rescale=1. / 255,
                             validation_split=0.2,
                             rotation_range=5,
                             zoom_range=0.1,
                             shear_range=0.05,
                             horizontal_flip=True)

train_datagen = datagen.flow_from_dataframe(train, directory=train_img,
                                            target_size=(224, 224),
                                            color_mode='rgb',
                                            class_mode='categorical',
                                            subset='training',
                                            batch_size=32,
                                            shuffle=True,
                                            x_col='image',
                                            y_col='labels')

validation_datagen = datagen.flow_from_dataframe(train, directory=train_img,
                                                 target_size=(224, 224),
                                                 color_mode='rgb',
                                                 class_mode='categorical',
                                                 subset='validation',
                                                 batch_size=32,
                                                 shuffle=True,
                                                 x_col='image',
                                                 y_col='labels')

test_datagen = datagen.flow_from_dataframe(test,
                                           directory=test_img,
                                           x_col='image',
                                           y_col=None,
                                           color_mode='rgb',
                                           target_size=(224, 224),
                                           class_mode=None,
                                           shuffle=False)
                                                

base_model = DenseNet201(include_top=False, weights=weights, input_shape=(224, 224, 3))
base_model.summary()

for layer in base_model.layers:
    layer.trainable = False

x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.2)(x)
x = Dense(64, activation='relu')(x)
predictions = Dense(6, activation='sigmoid')(x)

full_model = Model(inputs=base_model.input, outputs=predictions)

full_model.summary()

f1 = tfa.metrics.F1Score(num_classes=6, average='macro')
# tensorboard --logdir=log_dir
tb = C.TensorBoard(log_dir=log_dir,
                   histogram_freq=1,
                   embeddings_freq=1)

es = C.EarlyStopping(monitor='f1_score',
                     patience=5,
                     mode='max',
                     restore_best_weights=True)

full_model.compile(optimizer=Adam(1e-5),
                   loss='binary_crossentropy',
                   metrics=['accuracy', f1])

history = full_model.fit(train_datagen,
                         validation_data=validation_datagen,
                         epochs=30,
                         steps_per_epoch=train_datagen.samples // 128,
                         validation_steps=validation_datagen.samples // 128,
                         callbacks=[tb, es])

# unfreeze model

for layer in full_model.layers[:595]:
    layer.trainable = False

for layer in full_model.layers[371:]:
    layer.trainable = True

for layer in full_model.layers[595:]:
    layer.trainable = False

full_model.summary()

full_model.compile(optimizer=Adam(1e-5),
                   loss='binary_crossentropy',
                   metrics=['accuracy', f1])

history = full_model.fit(train_datagen,
                         validation_data=validation_datagen,
                         epochs=15,
                         steps_per_epoch=train_datagen.samples // 128,
                         validation_steps=validation_datagen.samples // 128,
                         callbacks=[tb, es])

full_model.save('plant_201.h5')

predictions = full_model.predict(test_datagen)
print(predictions)

import numpy as np

class_idx = []
for pred in predictions:
    pred = list(pred)
    temp = []
    for i in pred:
        if i > 0.4:
            temp.append(pred.index(i))
    if temp:
        class_idx.append(temp)
    else:
        temp.append(np.argmax(pred))
        class_idx.append(temp)
print(class_idx)

class_dict = train_datagen.class_indices


def get_key(val):
    for key, value in class_dict.items():
        if val == value:
            return key
print(class_dict)

sub_pred = []
for img in class_idx:
    img_pred = []
    for i in img:
        img_pred.append(get_key(i))
    sub_pred.append(' '.join(img_pred))
print(sub_pred)


sub = test[['image']]
sub['labels'] = sub_pred
sub.to_csv('submission.csv', index=False)
